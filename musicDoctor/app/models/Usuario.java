package models;

import play.*;
import play.data.validation.Required;
import play.db.jpa.*;

import javax.persistence.*;
import java.util.*;

@Entity
public class Usuario extends Model {
	@Required
    public String usuario;
    
    @Required
    public String password;
	
	@Required
	public String nombre;
    
    @Required 
    public String apellido;
    
    @Required
    public String email;
    
    @Required
    public String sexo;
    
    @Required
    public String telefono;
    
    @Required 
    public boolean activo;
    
    public Usuario(String usuario, String password, String nombre, String apellido, String email, String sexo, String telefono, boolean activo ){
    	this.usuario = usuario;
    	this.password = password;
    	this.nombre = nombre;
    	this.apellido = apellido;
    	this.email = email;
    	this.sexo = sexo;
    	this.telefono = telefono;
    	this.activo = activo;
    }
    
}
