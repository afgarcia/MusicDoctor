package models;

import play.*;
import play.data.validation.Required;
import play.db.jpa.*;

import javax.persistence.*;

import java.util.*;

@Entity
public class Link extends Model {
    @Required
	public String link;
    
    @Required
    public String descripcion;
    
    @Required
    public String genero;
    
    @ManyToOne 
    public TipoLink tipoLink;
    
    @OneToMany(mappedBy="link", cascade=CascadeType.ALL)
    public List<VoBo> voBo;
    
    public Link(String link, String descripcion, String genero, TipoLink tipoLink){
    	this.voBo = new ArrayList<VoBo>();
    	this.link = link;
    	this.descripcion = descripcion;
    	this.genero = genero;
    	this.tipoLink = tipoLink;
    }
    
}
