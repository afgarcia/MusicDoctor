package models;

import play.*;
import play.db.jpa.*;

import javax.persistence.*;

import java.util.*;

@Entity
public class RespTest extends Model {
    
    @ManyToOne
    public TestUsuario testUsuario;
    
    @ManyToOne
    public RespPregunta respPregunta;
    
    public RespTest(TestUsuario testUsuario, RespPregunta respPregunta){
    	this.testUsuario = testUsuario;
    	this.respPregunta = respPregunta;
    }
    
}
