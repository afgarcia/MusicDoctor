package models;

import play.*;
import play.data.validation.Required;
import play.db.jpa.*;

import javax.persistence.*;

import java.util.*;

@Entity
public class Pregunta extends Model {
    @Required
    public String pregunta;
    
    public String descripcion;
    
    @OneToMany(mappedBy="pregunta", cascade=CascadeType.ALL)
    public List<RespPregunta> respPregunta;
    
    public Pregunta(String pregunta, String descripcion){
    	this.respPregunta = new ArrayList<RespPregunta>();
    	this.pregunta = pregunta;
    	this.descripcion = descripcion;
    	
    }
    
}
