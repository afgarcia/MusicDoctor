package models;

import play.*;
import play.data.validation.Required;
import play.db.jpa.*;

import javax.persistence.*;

import java.util.*;

@Entity
public class VoBo extends Model {
    @Required 
    public boolean voBo;
    
    @ManyToOne
    public Link link;
    
    @ManyToOne
    public TipoLink tipoLink;
    
    @ManyToOne
    public TestUsuario testUsuario;
    
    public VoBo(boolean voBo, Link link, TipoLink tipoLink, TestUsuario testUsuario){
    	this.voBo = voBo;
    	this.link = link;
    	this.tipoLink = tipoLink;
    	this.testUsuario = testUsuario;
    	
    }
}
