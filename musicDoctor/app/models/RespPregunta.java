package models;

import play.*;
import play.data.validation.Required;
import play.db.jpa.*;

import javax.persistence.*;

import java.util.*;

@Entity
public class RespPregunta extends Model {
    @Required
    public boolean respuesta;
    
    @Required 
    public int valor;
    
    @ManyToOne
    public Pregunta pregunta;
    
    @OneToMany(mappedBy="respPregunta", cascade=CascadeType.ALL)
    public List<RespTest> respTest;
    
    public RespPregunta(boolean respuesta, int valor, Pregunta pregunta){
    	this.respTest = new ArrayList<RespTest>();
    	this.respuesta = respuesta;
    	this.valor = valor;
    	this.pregunta = pregunta;
    }
}
