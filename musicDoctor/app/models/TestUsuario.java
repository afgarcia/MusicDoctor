package models;

import play.*;
import play.data.validation.Required;
import play.db.jpa.*;

import javax.persistence.*;

import java.util.*;

@Entity
public class TestUsuario extends Model {
	@Required
	public Date fecha;
	
	@Required
	public Date hora;
	
	@Required 
	public int resultado;
	
	@ManyToOne
	public Usuario usuario;
	
	@OneToOne
	public TipoLink tipoLink;
	
	@OneToMany(mappedBy="testUsuario", cascade=CascadeType.ALL)
    public List<RespTest> respTest;
	
	@OneToMany(mappedBy="testUsuario", cascade=CascadeType.ALL)
    public List<VoBo> voBo;
	
	public TestUsuario(Date fecha, Date hora, int resultado, Usuario usuario, TipoLink tipoLink){
		this.respTest = new ArrayList<RespTest>();
		this.voBo = new ArrayList<VoBo>();
		this.fecha = fecha;
		this.hora = hora;
		this.resultado = resultado;
		this.usuario = usuario;
		this.tipoLink = tipoLink;
		
	}
	
    
}
