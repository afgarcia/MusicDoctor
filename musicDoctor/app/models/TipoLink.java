package models;

import play.*;
import play.data.validation.Required;
import play.db.jpa.*;

import javax.persistence.*;

import java.util.*;

@Entity
public class TipoLink extends Model {
    @Required
	public String tipo;
    
    @Required
    public String descripcion;
    
    @Required
    public int rangoPunteoMin;
    
    @Required
    public int rangoPunteoMax;
    
    @OneToMany(mappedBy="tipoLink", cascade=CascadeType.ALL)
    public List<Link> link;
    
    public TipoLink(String tipo, String descripcion, int rangoPunteoMin, int rangoPunteoMax){
    	this.link = new ArrayList<Link>();
    	this.tipo = tipo;
    	this.descripcion = descripcion;
    	this.rangoPunteoMin = rangoPunteoMin;
    	this.rangoPunteoMax = rangoPunteoMax;
    	
    }
    
}
