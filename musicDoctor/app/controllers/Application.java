package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;


public class Application extends Controller {
	
	@Before
    static void setConnectedUser() {
        if(Security.isConnected()) {
            Usuario user = Usuario.find("byUsuario", Security.connected()).first();
            renderArgs.put("user", user.usuario);
        }
    }
	
    public static void index() {
    	 render();
    }

}