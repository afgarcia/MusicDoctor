package controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import models.Link;
import models.Pregunta;
import models.RespPregunta;
import models.TestUsuario;
import models.TipoLink;
import models.Usuario;
import play.data.validation.Required;
import play.mvc.*;

@With(Secure.class)
public class Preguntas extends Controller {
	
	@Before
    static void setConnectedUser() {
        if(Security.isConnected()) {
            Usuario user = Usuario.find("byUsuario", Security.connected()).first();
            renderArgs.put("user", user.usuario);
        }
    }
    
	public static void listadoPreguntas() {
    	List<Pregunta> pregunta = Pregunta.findAll();
    	//System.out.println(pregunta.size());
        render(pregunta);
    }

    
    public static void respuestaPreguntas(@Required Map<String, String> respuestas) {
    	if (validation.hasErrors()){
    		List<Pregunta> pregunta = Pregunta.findAll();
    		//flash.error("Responda todas las preguntas!!!");
    		render("Preguntas/listadoPreguntas.html", pregunta);
    	}
    	int resultado=0;
        int id;
    	boolean valor;
        Iterator it = respuestas.entrySet().iterator();
		
        while (it.hasNext()) {
        	Map.Entry e = (Map.Entry)it.next();
        	//System.out.println(e.getKey() + " " + e.getValue());
        	
        	id=Integer.parseInt(e.getKey().toString());
        	valor = Boolean.parseBoolean(e.getValue().toString());
        	
        	if( (id == 2 || id == 18 || id==19) && valor == true ){
        		
        		resultado += valorRespuesta(id,valor);        		
        	}
        	else if(valor == true ){
        		
        		resultado += valorRespuesta(id,valor);;
        	}
        	else if(valor == false){
        		
        		resultado += valorRespuesta(id,valor);;
        	}
        	
        }
        System.out.println("\n\n");
        System.out.println(resultado);
        
        List<Link> links = links(resultado);
        render(resultado, links);
    }
    
    
    
    public static List<Link> links(int resultado){
    	   	
    	TipoLink tipoLink = TipoLink.find("Select t From TipoLink t Where t.rangoPunteoMin<=? and t.rangoPunteoMax>=?", resultado,resultado).first();
    	 
        if(tipoLink != null){
        	guardarTest(resultado, tipoLink);
        	List<Link> link = Link.find("byTipoLink",tipoLink).fetch();
        	return(link);
        }
        			
        return(null);
    }
    
    
    public static int guardarTest(int resultado, TipoLink tipoLink){
    	//System.out.println("entra guardarTest");
    	Calendar fecha = Calendar.getInstance();
    	Usuario usuario = Usuario.find("byUsuario", Security.connected()).first();
    	TestUsuario testUsuario = new TestUsuario(fecha.getTime(), fecha.getTime(), resultado, usuario, tipoLink);
    	
    	if(testUsuario.validateAndSave()){
    		//System.out.println(fecha.get);
    	}
    	return(0);
    }
    
    
    public static int valorRespuesta(long id, boolean resp){
   
    	Pregunta pregunta = Pregunta.findById(id);
    	RespPregunta respuesta = RespPregunta.find("Select r From RespPregunta r Where r.pregunta=? and r.respuesta=?",pregunta,resp).first();
    	if(respuesta != null){
    		
    		System.out.println("hay resp --> id="+ id);
    		System.out.println("valor="+ respuesta.valor);
    		return(respuesta.valor);
    	}
    	else{
    		System.out.println("no hay resp --> id="+ id);
    		//System.out.println("valor="+ respuesta.valor);
    		
    	}
    	return(0);
    }
}



