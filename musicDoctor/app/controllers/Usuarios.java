package controllers;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import models.Usuario;
import play.data.validation.Required;
import play.mvc.*;

public class Usuarios extends Controller {

    public static void index() {
        List<Usuario> usuario = Usuario.findAll();   
    	   	
    	render(usuario);
    }
    
    
    public static void login(){
    	
    	
    	render();
    }
    
    
    public static void registrarUsuario(@Required Map<String, String> usuario){
       Iterator it = usuario.entrySet().iterator();
		String id;
		String usua="";
		String password="";
		String nombre="";
		String apellido="";
		String email="";
		String sexo="";
		String telefono="";
		
        while (it.hasNext()) {
        	Map.Entry e = (Map.Entry)it.next();
            id= e.getKey().toString();
        	
            if(id == "txtusuario"){
               usua= e.getValue().toString();      	
            }
            else if(id == "txtpassword"){
            	password = e.getValue().toString();
            }
            else if(id == "txtnombre"){
            	nombre = e.getValue().toString();
            }
            else if(id == "txtapellido"){
            	apellido = e.getValue().toString();
            }
            else if(id == "txtemail"){
            	email =e.getValue().toString();
            }
            else if(id =="txtsexo"){
            	sexo = e.getValue().toString();
            }
            else if (id == "txttelefono"){
            	telefono = e.getValue().toString();
            }
            else{
            	 Usuario usu = new Usuario(usua,password,nombre,apellido,email,sexo,telefono, true);
                 
                 if (usu.validateAndCreate()){
                 	boolean resultado = true;
                 	String mensaje ="Usuario agregado";
                 	render(resultado, mensaje);
                 }
                 else{
                 	boolean resultado = true;
                 	String mensaje ="Usuario no se ha agregado";	
                   	render(resultado, mensaje);
                 }
            }
           
        }
    }
    
    // modifica y da de baja a un usuario con solo cambiar el valor del campo activo
    public static void modificaUsuario(@Required Map<String, String> usuario){
    	Iterator it = usuario.entrySet().iterator();
    	Iterator it2 = usuario.entrySet().iterator();
		String id;
		Usuario usu = null;
		
		//se inicia la busqueda del usuario, si este existe se sigue con la busqueda en el Map del restod e parametros
		 while (it.hasNext()) {
			Map.Entry e = (Map.Entry)it2.next();
			if(e.getValue().toString() == "txtusuario"){
	               
	               usu = Usuario.find("byUsuario", e.getValue().toString()).first();
	               if(usu != null){
	            	   usu.usuario = e.getValue().toString();
	               }
	               else{
	            	   boolean resultado =false;
	            	   String mensaje = "No se encontro el usuario";
	                   render(resultado, mensaje);
	               }
	            }
		}
		
		
        while (it.hasNext()) {
        	Map.Entry e = (Map.Entry)it.next();
            id= e.getKey().toString();
        	
            /*if(id == "txtusuario"){
               
               usu = Usuario.find("byUsuario", e.getValue().toString()).first();
               if(usu != null){
            	   usu.usuario = e.getValue().toString();
               }
               else{
            	   boolean resultado =false;
            	   String mensaje = "No se encontro el usuario";
                    render(resultado, mensaje);
               }
            }
            else*/
            if(id == "txtpassword"){
            	usu.password = e.getValue().toString();
            }
            else if(id == "txtnombre"){
            	usu.nombre = e.getValue().toString();
            }
            else if(id == "txtapellido"){
            	usu.apellido = e.getValue().toString();
            }
            else if(id == "txtemail"){
            	usu.email =e.getValue().toString();
            }
            else if(id =="txtsexo"){
            	usu.sexo = e.getValue().toString();
            }
            else if (id == "txttelefono"){
            	usu.telefono = e.getValue().toString();
            }
            else if (id == "chkactivo"){
            	usu.activo = Boolean.parseBoolean(e.getValue().toString());
            }
            else{
            	 if (usu.validateAndSave()){
                 	boolean resultado =false;
              	    String mensaje = "El usuario se ha modificado";
                 	render(resultado,mensaje);
                 }
                 else{
                 	boolean resultado =false;
              	    String mensaje = "No se ha podido modificar el usuario";
                 	render(resultado,mensaje);
                 }
            }
        }
    }

}
