
package controllers;

import java.util.List;

import models.TestUsuario;
import models.Usuario;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;

@With(Secure.class)
public class Dashboard extends Controller{
	
	@Before
    static void setConnectedUser() {
        if(Security.isConnected()) {
            Usuario user = Usuario.find("byUsuario", Security.connected()).first();
            renderArgs.put("user", user.usuario);
        }
    }
	
	public static void index(){
		Usuario  usuario = Usuario.find("byUsuario", Security.connected()).first();
		if(usuario != null){
			
			List<TestUsuario> testUsuario = TestUsuario.find("byUsuario", usuario).fetch();
			
			if(testUsuario != null){
				render(testUsuario);
			}
			
		}
		
				
		render();
	}

}
