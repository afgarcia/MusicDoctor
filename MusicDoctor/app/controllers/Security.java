package controllers;

import java.security.SecureRandom;

import models.Usuario;

import play.mvc.*;

public class Security extends Secure.Security {
    
    static boolean authenticate(String usuario, String password) {
        Usuario user = Usuario.find("byUsuario", usuario).first();
        return user != null && user.password.equals(password);
    }
}
